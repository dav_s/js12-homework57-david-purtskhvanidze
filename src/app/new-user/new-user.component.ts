import { Component, ElementRef, ViewChild } from '@angular/core';
import { User } from '../shared/user.model';
import { UsersService } from '../shared/users.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent {
  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('emailInput') emailInput!: ElementRef;
  @ViewChild('roleInput') roleInput!: ElementRef;
  @ViewChild('statusInput') statusInput!: ElementRef;

  constructor(public newUser: UsersService) {}

  createUser() {
    const name: string = this.nameInput.nativeElement.value;
    const email: string = this.emailInput.nativeElement.value;
    const role: string = this.roleInput.nativeElement.value;
    const status: boolean = this.statusInput.nativeElement.value;

    const user = new User(name, email, role, status);

    this.newUser.users.push(user);
  }
}
